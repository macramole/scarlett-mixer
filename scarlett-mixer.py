#!/usr/bin/env python3
import sys
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication, QMainWindow, QCheckBox, QGridLayout, QFrame, QHBoxLayout, QVBoxLayout, QLabel, QTabWidget,
							 QMenu, QMenuBar, QAction, QWidget, QSlider, QMessageBox, QComboBox, QPushButton)

import re
from subprocess import check_output, run
import json
import os

home = os.path.expanduser("~")
FILE_SAVE_PATH = f"{home}/.config/scarlett-mixer-saved.json"
CMD_GET_CARD_NUM = ["/usr/bin/aplay", "-l"]
# CARD_NUMBER is set on __init__
CMD_GET = ["/usr/bin/amixer","-c","CARD_NUMBER","get","'Mix {} Input {}'"]
CMD_SET = ["/usr/bin/amixer","-c","CARD_NUMBER","set","'Mix {} Input {}'","{}", "-q"]
CMD_SET_MAX = 172
VOL_MIN = -80
VOL_MAX = 6
VOL_RES = 0.5
MIXER_CHANNELS_TO_INPUTS = ["PCM 1", "PCM 2", "PCM 7", "PCM 8", "PCM 9", "PCM 10",
							"IN 1", "IN 2", "IN 3", "IN 4", "IN 5", "IN 6",
							"IN 7", "IN 8" ]
MIXERS = [
	["A","B"],
	["C","D"],
	["E","F"]
]
TAB_NAME_1 = "Main"
TAB_NAME_2 = "Headphones 1"
TAB_NAME_3 = "Headphones 2"

COUNT_STEREO_FADERS = 8

BYPASS_CARD_CHECK = False #coding without a card

def getCardNumber():
	try:
		out = check_output( CMD_GET_CARD_NUM ).decode("utf-8")
		ret = re.findall( r"card (\d):.*Scarlett", out )

		if len(ret) == 1:
			return int(ret[0])
		else:
			return None
	except:
		pass

def getCMD(mix, input):
	try:
		cmd = CMD_GET.copy()
		cmd[4] = cmd[4].format(mix,input)

		out = check_output( cmd )
		# print(out)

		ret = re.findall( r"\[([\-0-9.]*)dB\]", str(out) )

		return float(ret[0])
	except:
		return 0

def setCMD(mix, input, value):
	cmd = CMD_SET.copy()

	cmd[4] = cmd[4].format(mix,input)
	cmd[5] = cmd[5].format(value)

	run(cmd)
	# print(cmd)

def mapValue(value, leftMin, leftMax, rightMin, rightMax):
	# Figure out how 'wide' each range is
	leftSpan = leftMax - leftMin
	rightSpan = rightMax - rightMin

	# Convert the left range into a 0-1 range (float)
	valueScaled = float(value - leftMin) / float(leftSpan)

	# Convert the 0-1 range into a value in the right range.
	return rightMin + (valueScaled * rightSpan)

class StereoFaders(QWidget):
	# valueChanged = QtCore.pyqtSignal( int, str, int ) #not using leaving for future reference

	def __init__(self, index, mixers = ["A", "B"], link = False, parent=None):
		super(QWidget, self).__init__(parent)

		self.layout = QGridLayout()
		
		self.mixers = mixers

		self.sliderL = QSlider(Qt.Vertical)
		self.sliderL.setMinimum(0)
		self.sliderL.setMaximum(CMD_SET_MAX)
		self.sliderL.setSingleStep(1)
		# self.sliderL.mouseDoubleClickEvent = self.onSliderDoubleClick # can't get sender

		self.sliderR = QSlider(Qt.Vertical)
		self.sliderR.setMinimum(0)
		self.sliderR.setMaximum(CMD_SET_MAX)
		self.sliderR.setSingleStep(1)

		self.labelLdB = QLabel("0.00dB")
		self.labelLdB.setAlignment(Qt.AlignCenter)
		self.labelLdB.installEventFilter(self)
		self.labelRdB = QLabel("0.00dB")
		self.labelRdB.setAlignment(Qt.AlignCenter)
		self.labelRdB.installEventFilter(self)

		indexL = indexR = 0
		if type(index) is int:
			indexL = index
			indexR = index + 1
		else:
			indexL = index[0]
			indexR = index[1]
		self.channelL = self.createChannelCombo()
		self.channelL.setCurrentIndex( indexL )
		self.channelR = self.createChannelCombo()
		self.channelR.setCurrentIndex( indexR )

		self.btnLink = QPushButton("Link")
		self.btnLink.setCheckable(True)
		self.btnLink.setChecked(link)

		self.layout.addWidget( self.sliderL, 0, 0, alignment=QtCore.Qt.AlignHCenter )
		self.layout.addWidget( self.sliderR, 0, 1, alignment=QtCore.Qt.AlignHCenter )
		self.layout.addWidget(self.labelLdB, 1, 0, )
		self.layout.addWidget(self.labelRdB, 1, 1 )
		self.layout.addWidget(self.channelL, 2, 0)
		self.layout.addWidget(self.channelR, 2, 1)
		self.layout.addWidget(self.btnLink, 3, 0, 3, 2)

		self.setLayout(self.layout)
		self.initSliderValues()

		self.sliderL.valueChanged.connect(self.onSliderChanged)
		self.sliderR.valueChanged.connect(self.onSliderChanged)

	def createChannelCombo(self):
		c = QComboBox()
		# c.setAlignment(Qt.AlignCenter)
		# for i in range(0,10):
		# 	name = str(i)

		# 	if i < len(MIXER_CHANNELS_TO_INPUTS):
		# 		name = MIXER_CHANNELS_TO_INPUTS[i]

		# 	c.addItem(name)
		for name in MIXER_CHANNELS_TO_INPUTS:
			c.addItem(name)

		c.currentIndexChanged.connect(self.channelChanged)
		return c

	def channelChanged(self, value):
		if self.sender() == self.channelL:
			chan = str(self.channelL.currentIndex()+1).zfill(2)
			mixer = self.mixers[0]
			self.setValue(mixer, getCMD(mixer, chan))
		else:
			chan = str(self.channelR.currentIndex()+1).zfill(2)
			mixer = self.mixers[1]
			self.setValue(mixer, getCMD(mixer, chan))

	def initSliderValues(self):
		chan = str(self.channelL.currentIndex()+1).zfill(2)
		mixer = self.mixers[0]
		self.setValue(mixer, getCMD(mixer, chan))
		
		chan = str(self.channelR.currentIndex()+1).zfill(2)
		mixer = self.mixers[1]
		self.setValue(mixer, getCMD(mixer, chan))

	def setValue(self, mixer, dbValue):
		value = mapValue( dbValue, VOL_MIN, VOL_MAX, 0, CMD_SET_MAX )

		if mixer == self.mixers[0]:
			self.sliderL.setValue( int(value) )
			self.labelLdB.setText( str(dbValue) + "dB" )
		else :
			self.sliderR.setValue( int(value) )
			self.labelRdB.setText( str(dbValue) + "dB" )
	
	def setValues(self, dbValueL, dbValueR):
		self.setValue(self.mixers[0], dbValueL)
		self.setValue(self.mixers[1], dbValueR)

	def copyValuesToFader(self, targetFader):
		targetFader.channelL.setCurrentIndex( self.channelL.currentIndex() )
		targetFader.channelR.setCurrentIndex( self.channelR.currentIndex() )

		dbValueL = mapValue( self.sliderL.value(), 0, CMD_SET_MAX, VOL_MIN, VOL_MAX )
		dbValueR = mapValue( self.sliderR.value(), 0, CMD_SET_MAX, VOL_MIN, VOL_MAX )
		targetFader.setValues(dbValueL,dbValueR)

		targetFader.btnLink.setChecked( self.btnLink.isChecked() )

	def onSliderChanged(self, value):
		dbValue = mapValue( value, 0, CMD_SET_MAX, VOL_MIN, VOL_MAX )
		dbValue = "%.2f" % dbValue

		mixer = self.mixers[0]
		index = str(self.channelL.currentIndex()+1)
		if self.sender() == self.sliderR:
			mixer = self.mixers[1]
			index = str(self.channelR.currentIndex()+1)

		setCMD(mixer,index.zfill(2), value)

		if mixer == self.mixers[0]:
			self.labelLdB.setText( dbValue + "dB" )
		else:
			self.labelRdB.setText( dbValue + "dB" )

		if self.btnLink.isChecked():
			if mixer == self.mixers[0]:
				mixer = self.mixers[1]
			else:
				mixer = self.mixers[0]

			self.setValue(mixer, float(dbValue))

	def eventFilter(self, watched, event):
		if watched == self.labelLdB and event.type() == QtCore.QEvent.MouseButtonDblClick:
			if event.button() == 1:
				self.setValue(self.mixers[0], 0)
			else:
				self.setValue(self.mixers[0], VOL_MIN)

		if watched == self.labelRdB and event.type() == QtCore.QEvent.MouseButtonDblClick:
			if event.button() == 1:
				self.setValue(self.mixers[1], 0)
			else:
				self.setValue(self.mixers[1], VOL_MIN)

		return QWidget.eventFilter(self, watched, event)

	def getSaveData(self):
		return {
			"channels" : [ self.channelL.currentIndex(), self.channelR.currentIndex() ],
			"link" : self.btnLink.isChecked()
		}

class Window(QMainWindow):
	def __init__(self, parent=None):
		super(Window, self).__init__(parent)
		self.faders = {
			TAB_NAME_1 : [],
			TAB_NAME_2 : [],
			TAB_NAME_3 : []
		}

		self.setCardNumber()
		self.savedData = self.load()
		self.createUI()

		self.setWindowTitle("Scarlett Mixer")
		self.resize(400, 300)

	def setCardNumber(self):
		global CMD_GET, CMD_SET

		self.cardNumber = getCardNumber()

		if self.cardNumber != None:
			CMD_GET[2] = CMD_GET[2].replace("CARD_NUMBER", str(self.cardNumber))
			CMD_SET[2] = CMD_SET[2].replace("CARD_NUMBER", str(self.cardNumber))
		else:
			if not BYPASS_CARD_CHECK:
				msg = QMessageBox()
				msg.setIcon(QMessageBox.Critical)
				msg.setText("Scarlett Card not found")
				msg.setWindowTitle("Error")
				msg.setDetailedText("Cannot parse Scarlett in `aplay -l`")
				msg.exec()
				sys.exit(1)

	def onCopyTo(self, value):
		currentTabIndex = self.sender().parent().parent().parent().currentIndex()
		currentTabName = list(self.faders.keys())[currentTabIndex]
		copyToTabName = self.sender().itemText(value)

		for i, sourceFader in enumerate(self.faders[currentTabName]):
			targetFader = self.faders[copyToTabName][i]
			sourceFader.copyValuesToFader(targetFader)

	def populateTab(self, tab, which):
		mainLayout = QVBoxLayout()
		layout = QHBoxLayout()
		copyLayout = QHBoxLayout()

		# Copy layout
		lblCopy = QLabel("Copy to:")
		lblCopyFont = lblCopy.font()
		lblCopyFont.setPointSize(8)
		lblCopy.setFont(lblCopyFont)

		cboCopyTo = QComboBox()
		cboCopyToFont = cboCopyTo.font()
		cboCopyToFont.setPointSize(8)
		cboCopyTo.setFont(cboCopyToFont)

		if which == TAB_NAME_1:
			cboCopyTo.addItem(TAB_NAME_2)
			cboCopyTo.addItem(TAB_NAME_3)
		if which == TAB_NAME_2:
			cboCopyTo.addItem(TAB_NAME_3)
			cboCopyTo.addItem(TAB_NAME_1)
		if which == TAB_NAME_3:
			cboCopyTo.addItem(TAB_NAME_2)
			cboCopyTo.addItem(TAB_NAME_1)

		cboCopyTo.activated.connect(self.onCopyTo)

		copyLayout.addWidget(lblCopy)
		copyLayout.addWidget(cboCopyTo)
		copyLayout.insertStretch(0,20)

		# Line separator
		hline = QFrame()
		hline.setFrameShape(QFrame.HLine)
		hline.setFrameShadow(QFrame.Sunken)

		# Main mixer layout
		mixersIndex = list(self.faders.keys()).index(which)
		mixers = MIXERS[mixersIndex]

		if self.savedData == None:
			for i in range(0,COUNT_STEREO_FADERS * 2 - 1,2):
				fader = StereoFaders(i, mixers)
				self.faders[which].append(fader)
				layout.addWidget( fader )
		else:
			for i in range(0,COUNT_STEREO_FADERS):
				fader = None
				if i in self.savedData[mixersIndex]:
					f = self.savedData[mixersIndex][i]
					fader = StereoFaders( f["channels"], mixers, f["link"] )
				else:
					fader = StereoFaders(i, mixers)

				self.faders[which].append(fader)
				layout.addWidget( fader )

		## All together now
		mainLayout.addLayout(layout)
		mainLayout.addWidget(hline)
		mainLayout.addLayout(copyLayout)
		
		tab.setLayout( mainLayout )
		
	def populateMenu(self):
		menubar = self.menuBar()
		
		menu = QMenu("File", menubar)
		actionLoad = QAction("Load default configuration", menu)
		actionLoad.triggered.connect(self.onMenuLoadDefaultConfiguration)
		actionExit = QAction("Exit", menu)
		actionExit.triggered.connect(self.onMenuExit)
		menu.addAction(actionLoad)
		menu.addSeparator()
		menu.addAction(actionExit)
		
		menubar.addMenu(menu)
	
	def createUI(self):
		centralWidget = QWidget()
		mainLayout = QVBoxLayout()
		self.populateMenu()
		
		tabs = QTabWidget()
		tabMain = QWidget()
		tabHeadphone1 = QWidget()
		tabHeadphone2 = QWidget()

		tabs.addTab(tabMain, TAB_NAME_1)
		tabs.addTab(tabHeadphone1, TAB_NAME_2)
		tabs.addTab(tabHeadphone2, TAB_NAME_3)
		
		self.populateTab(tabMain, TAB_NAME_1)
		self.populateTab(tabHeadphone1, TAB_NAME_2)
		self.populateTab(tabHeadphone2, TAB_NAME_3)

		mainLayout.addWidget(tabs)
		centralWidget.setLayout(mainLayout)
		self.setCentralWidget(centralWidget)

	def load(self):
		if os.path.exists(FILE_SAVE_PATH):
			with open(FILE_SAVE_PATH) as json_file:
				data = json.load(json_file)
				return data
		else:
			return None

	def save(self):
		saveData = [[],[],[]]
		mixerIndexes = [MIXERS[0][0],MIXERS[1][0],MIXERS[2][0]]
		for tab in self.faders:
			for f in self.faders[tab]:
				saveData[ mixerIndexes.index(f.mixers[0]) ].append( f.getSaveData() )

		with open(FILE_SAVE_PATH, 'w') as outfile:
			json.dump(saveData, outfile)

	def closeEvent(self, event):
		self.save()

	def onMenuExit(self):
		self.close()
	def onMenuLoadDefaultConfiguration(self):
		ret = run(["alsactl", "restore", str(self.cardNumber), "-f", "./default-18i20.state"])
		if ret.returncode != 0:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Could not load default settings")
			msg.setWindowTitle("Error")
			msg.exec()
		else :
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Information)
			msg.setText("Default settings loaded successfully")
			msg.setWindowTitle("Information")
			msg.exec()



if __name__ == '__main__':
	app = QApplication(sys.argv)
	win = Window()
	win.resize(QtCore.QSize (600, 600))
	win.show()
	sys.exit(app.exec_())
