# Simple Scarlett Mixer for Linux

A simplified GUI for Focusrite Scarlett audio interfaces for Linux

![Screenshot](https://gitlab.com/macramole/scarlett-mixer/-/raw/master/screenshot.png)

## Motivation

I wanted to use the internal hardware routing of my Scarlett 18i20 in the simplest way for fulfilling my needs. I just need to quickly mix my analog inputs for zero latency monitoring.

For an advanced Scarlett Mixer make sure to check out [Geoffrey's awesome mixer](https://github.com/geoffreybennett/alsa-scarlett-gui)

## How does it work

### Tabs

Each tab is a group of stereo faders that controls a different mix. This way you can have 3 different stereo mixes for your Main output and each headphone.

### Stereo faders

Each stereo fader controls Left and Right channels of the mix.

You can select which channel to alter for each fader using the dropdown that is located below. You can also link the faders.

Double click on the dB labels will set the fader to 0db. The same with right button sets the fader to -80db.

It is possible to copy all settings on a tab to another one using the "Copy to" dropdown at the bottom right of the window.

Closing the window will save your settings and they will be loaded on next init.

Card number is guessed automagically.

## Example

Let's say I need to record a guitar player and a singer, each will have a headphone. I want them to hear what they are playing without latency. Let's say I'm using two mics connected to inputs 3, 4 for the guitar, and a third one connected to input 5 for the singer.

*Easy peasy*: I'll go to tab Headphone 1, select IN 3 and IN 4 in any fader, link them and put the volume up. That's it for the guitar. For the singer, I'll choose another pair of faders, select IN 5 for both faders, link them, put the volume up and that's it for Headphone 1. I'll copy settings from Headphone 1 to 2 and that's it.

But wait, guitar is too loud for the singer. I'll head to Headphone 2 tab, lower the volume of the guitar and that's it, two different mixes.

## Requirements

* python3
* PyQt5 (`sudo apt install python3-pyqt5`)

## Installing

There is no installation, just `chmod +x ./scarlett-mixer-stereo.py` and run: `./scarlett-mixer-stereo.py`. You can use something like `alacarte` for creating a shortcut in your main menu

### Initial setup

For this to be possible, a fixed routing must be set. To simplify this process I am providing an ALSA state file that contains all default routing you'll need. Loading it is very easy, just click `File -> Load default configuration` and that's it.

Basically what I'm doing is connecting:
* Mixers A and B to PCM 1 and 2 (main output)
* Mixers C and D to PCM 7 and 8 (first headphone)
* Mixers E and F to PCM 9 and 10 (second headphone)

Mixers have connected: PCM1, PCM2, Analog 1 ... to 8

## Testing

I've test this with a 18i20 2nd gen, it might not work for a smaller Scarlett, probably need some very little changes to the code. I'll update when I get one.